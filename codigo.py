    # -*- coding: utf-8 -*
import requests 
from bs4 import BeautifulSoup
from pathlib import Path
from PIL import Image

start_uri = "http://www.proveyourworth.net/level3/start"
activate_uri = "http://www.proveyourworth.net/level3/activate?statefulhash"
payload = "http://www.proveyourworth.net/level3/payload"
file_path = Path("./")

session = requests.Session()

def start_session(start_uri: str) -> None:
    session.get(start_uri)
    print(f'Hash: {session.cookies.get("PHPSESSID")}')

def get_hash(start_uri: str) -> str:
    request = session.get(start_uri)
    soup = BeautifulSoup(request.text, 'html.parser')
    return soup.find("input",{"name":"statefulhash"})['value']

def activate(activate_uri,get_hash: str) -> None:
    get_hash = get_hash(start_uri)
    session.get(activate_uri+f'={get_hash}')
    print(f"Hash: {get_hash}")

def get_image_to_sign(uri_image: str) -> bytes:
    request = session.get(uri_image,stream=True)
    image = request.raw
    return image

def sing_image(image: bytes) -> None:
    image = Image.open(image)
    image.save("image.jpg","JPEG")
    
def post_back_to(payload: str) -> None:
    payload = session.get(payload)
    post_uri = f"{payload.headers['X-Post-Back-To']}"
    file = {
        "code":open(file_path / "codigo.py","rb"),
        "resume":open(file_path / "MartinMSoria.pdf","rb"),
        "image":open(file_path / "image.jpg","rb")
    }
    data = {
        "email":"martinmiguel.soria@gmail.com",
        "name":"Martin Miguel Soria",
        "aboutme": "Argentino, Programador Dizque ... emprendedor",
        "code":"https://gitlab.com/00KuX/proveyourworth",
        "resume":"https://www.linkedin.com/in/mart%C3%ADn-miguel-soria-32003353/",
        "image":"https://www.geosurf.com/wp-content/uploads/2018/02/Webp.net-resizeimage-10-1.jpg"
    }
    request = session.post(post_uri, data=data, files=file)
    

if __name__ == '__main__':
    start_session(start_uri)
    activate(activate_uri,get_hash)
    sing_image(get_image_to_sign(payload))
    post_back_to(payload)